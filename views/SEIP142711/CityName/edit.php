<?php
require_once("../../../vendor/autoload.php");
use App\Message\Message;
if(!isset( $_SESSION)) session_start();

use App\CityName\CityName;
$objCity=new CityName();
$objCity->setData($_GET);
$oneData=$objCity->view('obj');

?>


<!DOCTYPE html>
<html lang="en">

    <head>

        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>City Name edit form</title>

        <!-- CSS -->
        <link rel="stylesheet" href="http://fonts.googleapis.com/css?family=Roboto:400,100,300,500">
        <link rel="stylesheet" href="../../../resource/assets/bootstrap/css/bootstrap.min.css">
        <link rel="stylesheet" href="../../../resource/assets/font-awesome/css/font-awesome.min.css">
		<link rel="stylesheet" href="../../../resource/assets/css/form-elements.css">
        <link rel="stylesheet" href="../../../resource/assets/css/style.css">

        <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
            <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
            <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
        <![endif]-->

        <!-- Favicon and touch icons -->
        <link rel="shortcut icon" href="../../../resource/assets/ico/favicon.png">
        <link rel="apple-touch-icon-precomposed" sizes="144x144" href="../../../resource/assets/ico/apple-touch-icon-144-precomposed.png">
        <link rel="apple-touch-icon-precomposed" sizes="114x114" href="../../../resource/assets/ico/apple-touch-icon-114-precomposed.png">
        <link rel="apple-touch-icon-precomposed" sizes="72x72" href="../../../resource/assets/ico/apple-touch-icon-72-precomposed.png">
        <link rel="apple-touch-icon-precomposed" href="../../../resource/assets/ico/apple-touch-icon-57-precomposed.png">

    </head>

    <body>
<div class="container-fluid wrapper">
    <div class="jumbotron header">
	
        <div class="row">
		  <div class="header-info col-md-7">
            <h2>Atomic Project</h2>
                
				<h3>City Name edit form</h3>
            </div>
            
        </div>

    <div>

    <hr class="hr-divider">
    <hr class="hr-divider">
    <div class="container-fluid wrapper">
        <div class="row">
            <div class="col-md-2">
                <ul class="nav atomic-side-nav nav-pills nav-stacked ">
                    <li role="presentation"><a href="#">Menu</a></li>
                    <li role="presentation" ><a href="#">Book Title  </a></li>
                    <li role="presentation" ><a href="#">Birthdate</a></li>
                    <li role="presentation" class="active"><a href="#">City</a></li>
                    <li role="presentation"><a href="#">Email</a></li>
                    <li role="presentation"><a href="#">Gender</a></li>
                    <li role="presentation"><a href="#">Hobbies</a></li>
                    <li role="presentation"><a href="#">Profile Picture</a></li>
                    <li role="presentation"><a href="#">Summary of Organization</a></li>
                </ul>
            </div>
            <div class="col-md-10 ">
                <div class="atomic-nav">
                    <nav class="navbar">
                        <div class="container">
<!--                            <div class="navbar-header">-->
<!--                                <span class="navbar-brand">Control</span>-->
<!--                            </div>-->
<!--                            <div class="btn-group-lg nav navbar-nav role="group" aria-label="...">-->
<!--                            <a href="create.php" class="navbar-btn btn btn-info">Add Item</a>-->
<!--                            <a href="" class="navbar-btn btn btn-warning">Trash Item</a>-->
<!--                            <a href="" class="navbar-btn btn btn-success">PDF Download</a>-->
<!--                            <a href="" class="navbar-btn btn btn-primary">Log out</a>-->
<!--                        </div>-->
                </div>
                </nav>
            </div>
            
            <form id="form" class="form-horizontal" action="update.php" method="post">
                <div class="form-group">
				<input type="hidden" name="id" value="<?php echo $oneData->id;?>">
                    <label for="name" class="col-sm-2 control-label">Person name</label>
                    <div class="col-sm-10">
                        <input type="text" class="form-control" id="person name" name="person name" value="<?php echo $oneData->person_name;?>">
                    </div>
                </div>
                <div class="form-group">
                    <label for="cityname" class="col-sm-2 control-label">Edit name of the City</label>
                    <select  id="cityname" name="city_name" value="<?php echo $oneData->city_name;?>">
                        <option <?php if($oneData->city_name=="Chittagong"):?>selected<?php endif ?>>Chittagong</option>
                                <option <?php if($oneData->city_name=="dhaka"):?>selected<?php endif ?>>dhaka</option>
                                <option <?php if($oneData->city_name=="Comilla"):?>selected<?php endif ?>>Comilla</option>
                                <option <?php if($oneData->city_name=="Natore"):?>selected<?php endif ?>>Natore</option>
                                <option <?php if($oneData->city_name=="rajshahi"):?>selected<?php endif ?>>rajshahi</option>
                            </select>
                </div>

                <div class="form-group">
                    <div class="col-sm-offset-2 col-sm-10">
                        <input type="submit" class="btn btn-success" value="submit">
                        <input type="update" class="btn btn-danger" value="update">
                    </div>
                </div>


				
            </form>
			
			
        </div>

        <?php echo Message::getMessage() ?>

    </div>
</div>


</div>

<!-- Javascript -->
<script src="../../../resource/assets/js/jquery-1.11.1.min.js"></script>
<script src="../../../resource/assets/bootstrap/js/bootstrap.min.js"></script>

<!--[if lt IE 10]>
<script src="../../../assets/js/placeholder.js"></script>
<![endif]-->



</body>

</html>
