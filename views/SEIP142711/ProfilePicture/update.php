<?php


require_once("../../../vendor/autoload.php");
use App\ProfilePicture\ProfilePicture;


if(!empty($_FILES['profile_picture']['name'])){

    $image_name =time().$_FILES['profile_picture']['name'];
    $temporary_location = $_FILES['profile_picture']['tmp_name'];
    move_uploaded_file($temporary_location,'img/'.$image_name);
    $_POST['img_name']=$image_name;

}


$objPP = new ProfilePicture();

$objPP->setData($_POST);

$objPP->update();
