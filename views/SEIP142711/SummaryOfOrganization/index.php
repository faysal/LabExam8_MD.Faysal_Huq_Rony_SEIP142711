<html>
<head>
<link rel="stylesheet" href="../../../resource/assets/bootstrap/css/bootstrap.min.css">
    <script src="../../../resource/assets/bootstrap/js/bootstrap.min.js"></script>
    </head>
<?php
require_once("../../../vendor/autoload.php");
use App\SummaryOfOrganization\SummaryOfOrganization;
use App\Message\Message;


$summary=new SummaryOfOrganization();
$dataSet=$summary->index("obj");
$serial=1;
echo "<table border='1px' align='center'>";
echo "<th>serial</th><th>id</th><th>Organization name</th><th>Summary of organization</th><th>Action</th>";

foreach($dataSet as $oneData) {
    echo "<tr>";
    echo "<td>$serial</td>";
    echo "<td>$oneData->id</td>";
    echo "<td>$oneData->organization_name</td>";
    echo "<td>$oneData->organization_summary</td>";

    $serial++;
    echo "
    <td>
    <a href='view.php?id=$oneData->id'><button class='btn-success'>view</button></a>
    <a href='edit.php?id=$oneData->id'><button class='btn-primary'>edit</button></a>
     <a href='trash.php?id=$oneData->id'><button class='btn-primary'>trash</button></a>
     <a href='delete.php?id=$oneData->id'><button class='btn-danger'>delete</button></a>
    </td>";
    echo "</tr>";

}

?>
</html>
