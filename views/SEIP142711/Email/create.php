<?php
require_once("../../../vendor/autoload.php");
use App\Message\Message;

if(!isset( $_SESSION)) session_start();


?>


<!DOCTYPE html>
<html lang="en">

    <head>

        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>Email - Create Form</title>

        <!-- CSS -->
        <link rel="stylesheet" href="http://fonts.googleapis.com/css?family=Roboto:400,100,300,500">
        <link rel="stylesheet" href="../../../resource/assets/bootstrap/css/bootstrap.min.css">
        <link rel="stylesheet" href="../../../resource/assets/font-awesome/css/font-awesome.min.css">
		<link rel="stylesheet" href="../../../resource/assets/css/form-elements.css">
        <link rel="stylesheet" href="../../../resource/assets/css/style.css">

        <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
            <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
            <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
        <![endif]-->

        <!-- Favicon and touch icons -->
        <link rel="shortcut icon" href="../../../resource/assets/ico/favicon.png">
        <link rel="apple-touch-icon-precomposed" sizes="144x144" href="../../../resource/assets/ico/apple-touch-icon-144-precomposed.png">
        <link rel="apple-touch-icon-precomposed" sizes="114x114" href="../../../resource/assets/ico/apple-touch-icon-114-precomposed.png">
        <link rel="apple-touch-icon-precomposed" sizes="72x72" href="../../../resource/assets/ico/apple-touch-icon-72-precomposed.png">
        <link rel="apple-touch-icon-precomposed" href="../../../resource/assets/ico/apple-touch-icon-57-precomposed.png">

    </head>

    <body>
<div class="container-fluid wrapper">
    <div class="jumbotron header">
	
        <div class="row">
		  <div class="header-info col-md-5">
            <h2>Atomic Project</h2>
              
            </div>
            
        </div>

    <div>

    <hr class="hr-divider">
    <hr class="hr-divider">
    <div class="container-fluid wrapper">
        <div class="row">
            <div class="col-md-2">
                <ul class="nav atomic-side-nav nav-pills nav-stacked ">
                    <li role="presentation"><a href="../index.php">Menu</a></li>
                    <li role="presentation" ><a href="../BookTitle/create.php">Book Title  </a></li>
                    <li role="presentation" ><a href="../Birthdate/create.php">Birthdate</a></li>
                    <li role="presentation"><a href="../CityName/create.php">City</a></li>
                    <li role="presentation"class="active"><a href="../Email/create.php">Email</a></li>
                    <li role="presentation"><a href="../Gender/create.php">Gender</a></li>
                    <li role="presentation"><a href="../Hobbies/create.php">Hobbies</a></li>
                    <li role="presentation"><a href=../ProfilePicture/create.php>Profile Picture</a></li>
                    <li role="presentation"><a href="../SummaryOfOrganization/create.php">Summary of Organization</a></li>
                </ul>

            </div>
            <div class="col-md-10 ">
                <div class="atomic-nav">
                    <nav class="navbar">
                        <div class="container">

                </div>
                </nav>
            </div>
            
            <form id="form" class="form-horizontal" action="store.php" method="post">
                <div class="form-group">
                    <label for="name" class="col-sm-2 control-label">Person name</label>
                    <div class="col-sm-10">
                        <input type="text" class="form-control" id="person_name" name="person_name" placeholder="Enter person name">
                    </div>
                </div>
                <div class="form-group">
                    <label for="email" class="col-sm-2 control-label">email</label>
                    <div class="col-sm-10">
                        <input type="email" class="form-control" id="email" name="email" placeholder="Enter Email">
                    </div>
                </div>

                <div class="form-group">
                    <div class="col-sm-offset-2 col-sm-10">
                        <input type="submit" class="btn btn-success" value="submit">
                        <input type="reset" class="btn btn-danger" value="reset">
                    </div>
                </div>



			<div id="message"><?php echo Message::getMessage() ?>
            </div>
				
            </form>
			
			
        </div>



    </div>
</div>


</div>

<!-- Javascript -->
<script src="../../../resource/assets/js/jquery-1.11.1.min.js"></script>
<script src="../../../resource/assets/bootstrap/js/bootstrap.min.js"></script>

<!--[if lt IE 10]>
<script src="../../../assets/js/placeholder.js"></script>
<![endif]-->



</body>

</html>
